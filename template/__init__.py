import os
import re

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse

__version__ = "0.1.0"

class RouteList(list):
    def append(self, item):
        entry = self.pop()
        super().append(item)
        super().append(entry)

class Server(FastAPI):
    def __init__(self, entry, *args, **kwargs):
        super().__init__(*args, **kwargs)
        super().add_api_route("/{path:path}", entry)
        self.router.routes = RouteList(self.router.routes)

    @classmethod
    def create(cls, build):
        async def entry(path):
            if path and os.path.exists(file := os.path.join(build, path)):
                return FileResponse(file)
            return FileResponse(os.path.join(build, "index.html"))

        app = cls(entry)

        app.add_middleware(
            CORSMiddleware,
            allow_origin_regex=re.compile(
                f"https?://.*.?({os.environ.get('HOST')}|localhost(:.*)?)"
            ),
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )
        
        return app